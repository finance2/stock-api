package com.finance.controller;

import java.util.List;

import com.finance.dto.response.StockResponse;
import com.finance.service.StockService;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("stock")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StockController {

	@Inject
	private StockService service;

	@GET
	public List<StockResponse> hello() {
		return service.listStocks();
	}
	
	@GET
	@Path("hello2")
	public String hello2() {
		return "Hello 2";
	}
}
