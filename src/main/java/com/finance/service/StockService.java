package com.finance.service;

import java.util.List;

import com.finance.dto.response.StockResponse;

import jakarta.ejb.Stateless;

@Stateless
public class StockService {

	public List<StockResponse> listStocks() {
		return List.of(new StockResponse("BMIN", 4.8, 1.2));
	}
}
