package com.finance.dto.response;

public record StockResponse(
		String ticker,
		Double price,
		Double marginEbit
		) {}
